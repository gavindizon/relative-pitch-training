using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WorldInteraction : MonoBehaviour
{

    public PostSignLogic psl;
    private PopUpSystem popUpSystem;

    public CastFieldLogic cfl;
    public GameObject canvas;
    public GameObject dialogueBox;
    public DialogueFieldLogic dfl;

    // for spellcasting ui
    private GameObject spellCast;
    private Animator scAnim;
    private TextMeshPro scText;
    private AudioSource bgm;
    private float tempVol;

    bool isKeyDown = false;
    bool inRange = false;
    bool cflInRange = false;

    public void Start()
    {
        canvas = GameObject.Find("Canvas");
        popUpSystem = GameObject.Find("PopUpBox").GetComponent<PopUpSystem>();
        spellCast = GameObject.Find("SpellText");
        scAnim = spellCast.GetComponent<Animator>();
        scText = spellCast.GetComponent<TextMeshPro>();
        bgm = GameObject.Find("Background Music").GetComponent<AudioSource>();
    }

    public void Update()
    {
        // opening sign
        if (Input.GetKeyDown(KeyCode.E) && inRange && psl)
        {
            if (isKeyDown)
            {
                isKeyDown = false;
                Debug.Log("Double E, cancel");
            }
            else
            {
                isKeyDown = true;
                popUpSystem.PopUp(psl.getText());
            }
        }



        // closing sign
        if (Input.GetKeyDown(KeyCode.Q) && inRange && psl)
        {
            isKeyDown = false;
            popUpSystem.PopDown();
        }

        if (Input.GetKeyDown(KeyCode.E) && cflInRange && cfl)
        {
            cfl.ResetNotes();

            List<string> notesWithExtension = new List<string> { };
            for (int i = 0; i < cfl.notes.Count; i++)
                notesWithExtension.Add(cfl.notes[i] + ".mp3");

            // playing the notes to be mimicked by the user
            StartCoroutine(AudioController.PlaySequence("Notes/", notesWithExtension, 1.2f));

        }

        if (scAnim.GetCurrentAnimatorStateInfo(0).IsName("Idle") ||
         scAnim.GetCurrentAnimatorStateInfo(0).IsName("Show") && scAnim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1.0f)
            CastSpell();
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Post Sign"))
        {
            inRange = true;
            psl = collision.gameObject.GetComponent<PostSignLogic>();
        }
        else if (collision.gameObject.CompareTag("Cast Area"))
        {
            cfl = collision.gameObject.GetComponent<CastFieldLogic>();
            cfl.setIsAllowed(true);
            cflInRange = true;
            if (bgm)
            {
                tempVol = bgm.volume;
                bgm.volume = 0.0F;
            }
        }
        else if (collision.gameObject.CompareTag("Dialogue Area"))
        {
            dfl = collision.gameObject.GetComponent<DialogueFieldLogic>();
            if (!dfl.isActivated)
            {
                canvas.SetActive(true);
                dialogueBox = GameObject.Find("DialogueBox");
                dialogueBox.GetComponent<Dialogue>().SetDialogue(dfl.lines, dfl.saidBy);
                dfl.isActivated = true;
            }
        }

        else
        {
            Debug.Log("I'm not touching the sign");
        }
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (psl)
        {
            popUpSystem.PopDown();
            inRange = false;
        }

        if (cfl)
        {
            cflInRange = false;
            cfl.setIsAllowed(false);
            bgm.volume = tempVol;
        }
    }


    public void CastSpell()
    {

        bool isNote = false;
        string chr = Input.inputString;
        string note = "";
        if (!string.IsNullOrEmpty(chr))
        {

            if (Input.GetKey(KeyCode.RightShift) || Input.GetKey(KeyCode.LeftShift))
            {
                switch (chr.ToUpper())
                {
                    case "U":
                        note = "Do_Octave";
                        isNote = true;
                        break;
                    case "I":
                        note = "Re_Octave";
                        isNote = true;
                        break;
                    case "O":
                        note = "Mi_Octave";
                        isNote = true;
                        break;
                    case "P":
                        note = "Fa_Octave";
                        isNote = true;
                        break;
                    case "J":
                        note = "Sol_Octave";
                        isNote = true;
                        break;
                    case "K":
                        note = "La_Octave";
                        isNote = true;
                        break;
                    case "L":
                        note = "Ti_Octave";
                        isNote = true;
                        break;

                }
            }
            else
            {
                switch (chr.ToUpper())
                {
                    case "U":
                        note = "Do";
                        isNote = true;
                        break;
                    case "I":
                        note = "Re";
                        isNote = true;
                        break;
                    case "O":
                        note = "Mi";
                        isNote = true;
                        break;
                    case "P":
                        note = "Fa";
                        isNote = true;
                        break;
                    case "J":
                        note = "Sol";
                        isNote = true;
                        break;
                    case "K":
                        note = "La";
                        isNote = true;
                        break;
                    case "L":
                        note = "Ti";
                        isNote = true;
                        break;

                }
            }
            

            if (isNote)
            {
                // plays sound
                StartCoroutine(AudioController.PlayAudio("Notes/", note + ".mp3"));

                Debug.Log(cflInRange);
                if (cfl && cflInRange)
                    cfl.AddPlayerNotes(note);

                scText.SetText(note);
                scAnim.SetTrigger("cast");
            }
        }
    }
}
