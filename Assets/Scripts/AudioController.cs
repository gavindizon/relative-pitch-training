using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class AudioController : MonoBehaviour
{
    private static AudioSource source;
    private static AudioClip clip;
    private static AudioSource bgm;

    private void Start()
    {
        source = gameObject.AddComponent<AudioSource>();
        bgm = GameObject.Find("Background Music").GetComponent<AudioSource>();
        
    }


    private static void PlayAudioFile()
    {
        source.clip = clip;
        source.PlayOneShot(clip);
    }
    // don't use IEnumerator to fix delay
    public static IEnumerator PlayAudio(string path, string filename)
    {
        string audioToLoad = string.Format(Application.streamingAssetsPath + "/SFX/" + path + "{0}", filename);

        audioToLoad = audioToLoad.Replace(" ", "%20");

 //       Debug.Log(audioToLoad);
        Uri audioUri = new Uri(audioToLoad);

        Debug.Log("AHHH: " + audioUri.Host);

        using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(audioUri, AudioType.MPEG))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log(www.error);
            }
            else
            {
                clip = DownloadHandlerAudioClip.GetContent(www);
                Debug.Log(clip);
                PlayAudioFile();
            }
        }

    }

    public static IEnumerator PlaySequence(string path, List<string> filenames, float waitTime)
    {

        for (int i = 0; i < filenames.Count; i++)
        {
            if (i != 0)
                yield return new WaitForSeconds(waitTime);

            string audioToLoad = string.Format(Application.streamingAssetsPath + "/SFX/" + path + "{0}", filenames[i]);
            audioToLoad = audioToLoad.Replace(" ", "%20");

            Uri audioUri = new Uri(audioToLoad);

            Debug.Log("AHHH: " + audioUri.Host);



            using (UnityWebRequest www = UnityWebRequestMultimedia.GetAudioClip(audioUri, AudioType.MPEG))
            {
                yield return www.SendWebRequest();

                if (www.result == UnityWebRequest.Result.ProtocolError || www.result == UnityWebRequest.Result.ConnectionError)
                {
                    Debug.Log(www.error);
                }
                else
                {
                    clip = DownloadHandlerAudioClip.GetContent(www);
                    Debug.Log(clip);
                    PlayAudioFile();
                }
            }


        }
    }

}
