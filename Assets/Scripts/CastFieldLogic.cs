﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(BoxCollider2D))]
public class CastFieldLogic : MonoBehaviour
{
    public enum InteractionType { NONE, Examine }
    public InteractionType type;
    public UnityEvent customEvent;
    public TextMesh counter;

    [SerializeField] public List<string> notes;
    private List<string> playerNotes;
    private bool isAllowed;
    public bool isActivated;

    private void Start()
    {
        isAllowed = false;
        isActivated = false;
        playerNotes = new List<string> { };
        counter = GameObject.Find("/"+ gameObject.name + "/Count").GetComponent<TextMesh>();
        counter.text = String.Format("{0}", notes.Count);
    }

    public bool getIsAllowed()
    {
        return isAllowed;
    }
    public void setIsAllowed(bool a)
    {
        this.isAllowed = a;
    }

    public void Reset()
    {
        GetComponent<Collider2D>().isTrigger = true;
        gameObject.layer = 7;
    }

    public void Interact()
    {
        customEvent.Invoke();
    }

    private int NotesToInt(string note)
    {
        switch (note.ToLower())
        {
            case "do":
                return 0;
            case "re":
                return 1;
            case "mi":
                return 2;
            case "fa":
                return 3;
            case "sol":
                return 4;
            case "la":
                return 5;
            case "ti":
                return 6;
            case "do_octave":
                return 7;
            case "re_octave":
                return 8;
            case "mi_octave":
                return 9;
            case "fa_octave":
                return 10;
            case "sol_octave":
                return 11;
            case "la_octave":
                return 12;
            case "ti_octave":
                return 13;
            default:
                return -1;
        }

    }

    private List<int> ConvertToInterval(List<string> seq)
    {

        List<int> intervals = new List<int> { };

        for(int i = 0; i < seq.Count - 1; i++)
            intervals.Add(NotesToInt(seq[i+1]) - NotesToInt(seq[i]));

        return intervals;
    }



    public void AddPlayerNotes(string note)
    {
        if (isAllowed)
        {
            if (notes.Count == playerNotes.Count)
                playerNotes.Clear();

            playerNotes.Add(note);

            // check if equal
            if (ConvertToInterval(notes).SequenceEqual(ConvertToInterval(playerNotes)))
            {
                counter.text = "";
                Interact();
            }
            else if (notes.Count > playerNotes.Count)
            {
                counter.text = String.Format("{0}", notes.Count - playerNotes.Count);
            }
            else
            {
                counter.text = String.Format("{0}", notes.Count);
            }
        }
        else
        {
            Debug.Log("Nope");
        }

    }
    public void ResetNotes()
    {
        playerNotes.Clear();
        counter.text = String.Format("{0}", notes.Count);

    }

}