using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostSignLogic : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private string text;

    public string getText()
    {
        return text;
    }
    
}
