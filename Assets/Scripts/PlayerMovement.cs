using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private Animator animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private BoxCollider2D bc;
    private GameObject canvas;
    
    private float dirX = 0f;

    // Add 
    [SerializeField] private float MOVE_SPEED = 7f;
    [SerializeField] private float JUMP_FORCE = 14f;
    [SerializeField] private LayerMask jumpableGround;

    private enum MovementState
    {
        idle, running, jumping, falling
    }


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Player movement initialized");
        canvas = GameObject.Find("Canvas");

        // player physics
        rb = GetComponent<Rigidbody2D>();
        // player sprite
        sr = GetComponent<SpriteRenderer>();
        // player animation
        animator = GetComponent<Animator>();

        bc = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (canvas.activeSelf == false)
        {
            dirX = Input.GetAxis("Horizontal");
            rb.velocity = new Vector2(dirX * MOVE_SPEED, rb.velocity.y);
        // or Input.GetKeyDown("Jump") -> GetButtonDown is Unity native
        if (Input.GetButtonDown("Jump") && isGrounded())
            rb.velocity = new Vector2(rb.velocity.y, JUMP_FORCE);
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }

        UpdateAnimationState();


    }

    private void UpdateAnimationState()
    {
        MovementState state;


        if (dirX > 0f)
        {
            state= MovementState.running;
            sr.flipX = false;
        }
        else if (dirX < 0f)
        {
            state = MovementState.running;
            sr.flipX = true;
        }
        else
        {
            state = MovementState.idle;
        }

        if(rb.velocity.y > 0.0001f)
        {
            state = MovementState.jumping;
        }
        else if(rb.velocity.y < -0.1f)
        {
            state = MovementState.falling;
        }

        if (canvas.activeSelf == true)
            state = MovementState.idle;


        animator.SetInteger("state", (int) state);
    }


    private bool isGrounded()
    {
        return Physics2D.BoxCast(bc.bounds.center, bc.bounds.size, 0f, Vector2.down, .1f, jumpableGround);
    }
}
