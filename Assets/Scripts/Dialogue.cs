using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Dialogue : MonoBehaviour
{
    public TextMeshProUGUI textComponent;
    public TextMeshProUGUI saidByComponent;
    public string[] lines;
    public string saidBy;
    public float textSpeed;


    private int index;
    
    // Start is called before the first frame update
    void Start()
    {
        textComponent.text = string.Empty;
        saidByComponent.text = saidBy;
        StartDialogue();

        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if(textComponent.text == lines[index])
            {
                NextLine();

            }
            else
            {
                StopAllCoroutines();
                textComponent.text = lines[index];
            }

        }


    }
    
    void StartDialogue()
    {
        index = 0;
        StartCoroutine(TypeLine());

    }

    IEnumerator TypeLine()
    {
        foreach (char c in lines[index].ToCharArray())
        {
            textComponent.text += c;
            yield return new WaitForSeconds(textSpeed);
        }
    }

    void NextLine()
    {
        if(index < lines.Length - 1)
        {
            index++;
            textComponent.text = string.Empty;
            StartCoroutine(TypeLine());
        }
        else
        {
            GameObject.Find("Canvas").SetActive(false);
//            gameObject.SetActive(false);
        }
    }

    public void SetDialogue(string[] newLines, string saidBy)
    {
        textComponent.text = string.Empty;
        saidByComponent.text = saidBy;
        lines = newLines;
        index = 0;
        StartCoroutine(TypeLine());

    }

}
